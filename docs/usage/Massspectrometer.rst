Mass Spectrometer
=================




.. autoclass:: ttlab.MassSpectrometer
    :members:

Example
-------
Example with mass spectrometer data:

.. code-block:: python

    from ttlab import MassSpectrometer


    # Create a mass spec object
    filename = 'path/filename.asc'
    MS = MassSpectrometer(filename)

    # Check what gases are included in the data
    print(MS.gases)

    # Plot one of the gases, using matploltlib, returns the axes
    ax = MS.plot_gas('Ar')

    # Get the ion current and the relative time for the gas, returns np arrays with the data
    ion_current_argon = MS.get_ion_current('Ar')
    time_relative = MS.get_relative_time('Ar')
