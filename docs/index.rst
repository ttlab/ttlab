.. ttlab documentation master file, created by
   sphinx-quickstart on Fri Feb  1 20:37:49 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to ttlab's documentation!
=================================
.. image:: https://bytebucket.org/tt-lab/ttlab/raw/07af7e037611c6d2b47aef425bc7e078519c04ce/assets/header.png?token=9425964a76732a6a54d81fb58935ae6dc8acbfa4


.. toctree::
   :maxdepth: 2
   :caption: Contents:

   usage/XPS
   usage/Massspectrometer
   ...



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
